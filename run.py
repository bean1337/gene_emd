import numpy as np
import pandas as pd
import os
import re
import time
import zipfile
from tqdm import tqdm 
import subprocess

from transformers import AutoTokenizer, AutoModel
import subprocess



class Text_Embeddings(object):
    

    def __init__(self, model_name):
      
        self.tokenizer = AutoTokenizer.from_pretrained(
          model_name)

        self.model     = AutoModel.from_pretrained(
          model_name)


        """ sentences : list of lists        : [['hello', 'world' ],['word', 'python']]
            vocab     : List of unique words : ['hello', 'world'] """

    def transformer_embeddings(self, data):
      """any huggin face model : https://huggingface.co/models """
  
      label_e = self.get_tr_embeddings(data, self.tokenizer, self.model).squeeze()

      return {'array' : np.array(label_e), 'vocab' : self.vocab}
    

    def get_tr_embeddings(self, word, tokenizer, model):

      inputs = tokenizer(word, return_tensors='pt', padding=True, truncation=True)
      outputs = model(**inputs)
      result = outputs.pooler_output
      return result.cpu().detach().numpy()



# """ Test cases 
# sentences = [['this', 'is', 'the', 'first', 'sentence', 'for', 'word2vec'],
# 			['this', 'is', 'the', 'second', 'sentence'],
# 			['yet', 'another', 'sentence'],
# 			['one', 'more', 'sentence'],
# 			['and', 'the', 'final', 'sentence']]
# vocab = list(set([m for k in sentences for m in k]))
# mo = Text_Embeddings(sentences, vocab)
# mo_w = mo.word2vec_model()
# mo_w['array'].shape
# mo_w['array']
# # universal sentence encoder
# mo_w = mo.use_embd('use_l',2)
# mo_w
# # any transformer model
# emd = mo.transformer_embeddings("roberta-large-mnli")
# emd


def save_embeddings(data, outfile):

  W     = data['array']
  words = data['vocab']
  with open(outfile, 'w') as o:
    #pad token already included
    for i in tqdm(range(len(words))):
      line = [words[i]]
      line.extend([str(d) for d in W[i]])
      o.write(" ".join(line) + "\n")


import pandas as pd
df = pd.read_csv('vocab.csv',names=['vocab'],header=None)
df_l = list(df['vocab'])

tf_models = ["sentence-transformers/LaBSE",
"sentence-transformers/bert-base-nli-mean-tokens",
"sentence-transformers/paraphrase-xlm-r-multilingual-v1",
"sentence-transformers/paraphrase-multilingual-mpnet-base-v2",
"allenai/scibert_scivocab_uncased",
"xlm-roberta-base","roberta-large",
"roberta-base","bert-large-uncased","albert-base-v2",
"roberta-large-mnli","deepset/sentence_bert",
"xlm-roberta-large","kamalkraj/bioelectra-base-discriminator-pubmed",
"bert-base-uncased",
"monologg/biobert_v1.0_pubmed_pmc",
"microsoft/BiomedNLP-PubMedBERT-base-uncased-abstract-fulltext",
"microsoft/BiomedNLP-PubMedBERT-base-uncased-abstract",
"minhpqn/bio_roberta-base_pubmed",
"bionlp/bluebert_pubmed_uncased_L-12_H-768_A-12",
"bionlp/bluebert_pubmed_mimic_uncased_L-24_H-1024_A-16",
"sentence-transformers/roberta-large-nli-stsb-mean-tokens",
"sentence-transformers/bert-large-nli-mean-tokens",
"bionlp/bluebert_pubmed_mimic_uncased_L-12_H-768_A-12",
"adamlin/NCBI_BERT_pubmed_mimic_uncased_large_transformers",
"TheLongSentance/MIMIC-III-t5-large-v1",
"adamlin/NCBI_BERT_pubmed_mimic_uncased_base_transformers",
"mnaylor/bigbird-base-mimic-mortality",
'ian/BlueBERT-Finetuned-MIMIC-III-ICD-9-Classification']



def split_d(da, splits_):

  print(f'total {len(da)}')
  le_d = len(da)
  spl  = le_d // splits_
  data_s = []
  m=0
  for i in range(splits_):
    if i!=splits_-1:
      data_s.append(da[m:m+spl])
      m+=spl
    else:
      data_s.append(da[m:])
  return data_s



def full_encod(mo, model_name, data_full, splits_name):

  data_splits_da = split_d(data_full, splits_name)
  da_final = {'array': [], 'vocab': []}
  for da_kl in tqdm(data_splits_da):

    mo_w = mo.transformer_embeddings(da_kl)
    da_final['array'].append(mo_w['array'])
    da_final['vocab'].extend(mo_w['vocab'])
    print(model_name, mo_w['array'].shape)

  da_final['array'] = np.concatenate(da_final['array'])
  print(da_final['array'].shape)
  print(f"deleting model{model_name}")
  subprocess.call(['rm', '-rf', '/home/admin/.cache/huggingface/transformers/*'])
  print("deleted")
  return da_final



for k in tf_models:
  try:
    print(f"model_now {k}")
    print("\n")

    mo         = Text_Embeddings(k)
    data_saved = full_encod(mo, k , df_l, 30)
    dim_v = data_saved['array']
    name_ = k.split('/')[-1]
    print(name_, dim_v.shape)
    save_embeddings(data_saved, f'/tf_embds/transformer_{name_}_{dim_v}.embed')
  except Exception as e:
    print("_______________________________")
    print(f"No working model model_now {k} {e}")
    print("_______________________________")


# def data_s(da):
#   le_d = len(da)
#   spl  = le_d // 10
#   data_s = {}

#   for i in range(spl):
#     data_s[i] = da[i:i+1]
